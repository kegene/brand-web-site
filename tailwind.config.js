/** @type {import('tailwindcss').Config} */
/* eslint @typescript-eslint/no-var-requires: "off" */
const defaultTheme = require('tailwindcss/defaultTheme')

module.exports = {
  purge: [
    './public/*.html',
    './public/**/*.html',
    './src/**/*.{ts,tsx,js,jsx,vue}'
  ],
  darkMode: false, // or 'class'
  theme: {
    extend: {
      fontSize: {
        '1-5xl': ['1.35rem', { lineHeight: '1.85rem' }]
      },
      borderWidth: {
        3: '3px'
      },
      borderRadius: {
        '2lg': '0.675rem'
      },
      inset: {
        screenY: '100vh',
        '-screenY': '-100vh',
        screenX: '100vw',
        '-screenX': '-100vw'
      }
    },
    screens: {
      'max-2xl': { max: '1535px' },
      'max-xl': { max: '1279px' },
      'max-lg': { max: '1023px' },
      'max-md': { max: '767px' },
      'max-sm': { max: '639px' },
      ...defaultTheme.screens
    }
  },
  variants: {
    extend: {
      display: ['group-hover']
    }
  },
  plugins: [
    require('@tailwindcss/aspect-ratio')

  ]
}

declare module '*.vue' {
  import Vue from 'vue'
  export default Vue
}
declare module 'lodash' {
  import lodash from 'lodash'
  export default lodash
}
declare module 'lodash/*' {
  import fp from 'lodash/fp/**'
  export default fp
}
declare module 'vue-kinesis' {
  import { KinesisContainer, KinesisElement, KinesisAudio, KinesisDistance, kinesisKinesisScroll } from 'vue-kinesis'
  export {
    KinesisContainer,
    KinesisElement,
    KinesisAudio,
    KinesisDistance,
    kinesisKinesisScroll
  }
}
declare module '*.png';
declare module '*.svg';
declare module '*.jpeg';
declare module '*.jpg';
declare module '*.js';
declare module 'tailwindcss/resolveConfig';

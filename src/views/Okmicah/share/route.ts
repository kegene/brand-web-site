import { Location } from 'vue-router'

export interface Nav {
  to: Location;
  text: string;
}

export const nav: Nav[] = [
  {
    to: { name: 'OkmicahOurStory' },
    text: 'Our Story'
  },
  {
    to: { name: 'OkmicahOurWork' },
    text: 'Our Work'
  },
  {
    to: { name: 'OkmicahServices' },
    text: 'Services'
  },
  {
    to: { name: 'OkmicahWorkWithUs' },
    text: 'Work With Us'
  }
]

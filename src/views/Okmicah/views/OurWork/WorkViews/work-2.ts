import { WorkProp } from './type'
import Img1 from '@/assets/okmicah/client-work/img-client-work-11.png'
import Img2 from '@/assets/okmicah/client-work/img-client-work-12.png'
import Img3 from '@/assets/okmicah/client-work/img-client-work-13.png'
import Img4 from '@/assets/okmicah/client-work/img-client-work-14.png'

export default {
  title: 'Yes, Chef!',
  paragraph: 'Yes, Chef! is a queer-owned and operated Brand Design + Strategy-focused studio based in Los Angeles, California. They are a nod to unforgettable culinary experiences, the power of food & the culinary experts whose creations make us feel good.',
  whatWeDid: [1, 2, 3],
  howWeDidIt: 'We always have fun with our projects, but some projects bring the fun to you! Working with a brand like Yes, Chef! was a literal dream come true. Nick came to us wanting a unique website that\'s both playful and beautiful. It was a joy getting the chance to break the norm and create a website that communicates their studio\'s fun-loving approach to design.',
  bannerIframe: Img1,
  images: [
    Img1,
    Img2,
    Img3,
    Img4
  ]
} as WorkProp

import { WorkProp } from './type'
import Img1 from '@/assets/okmicah/client-work/img-client-work-19.png'
import Img2 from '@/assets/okmicah/client-work/img-client-work-15.png'
import Img3 from '@/assets/okmicah/client-work/img-client-work-16.png'
import Img4 from '@/assets/okmicah/client-work/img-client-work-17.png'
import Img5 from '@/assets/okmicah/client-work/img-client-work-18.png'

export default {
  title: 'Fred & Co',
  paragraph: 'Fred&Co. is the creative practice of Molly O’Neill based in Minneapolis. She is a visual designer who can execute anything from a huge coffee table book design to a mural install. Today, she works with a small team to ensure that Fred&Co. can maximize the creative possibilities for each and every project that knocks on their door.',
  whatWeDid: [1, 3],
  howWeDidIt: 'We were stoked when Molly, founder of Fred & Co, asked us to build their studio\'s new website. As a fellow queer owned business, we took their clear vision for the brand and translated that into a website that communicates their values, love for typography, and incredible energy.',
  bannerImage: Img1,
  images: [
    Img2,
    Img3,
    Img4,
    Img5
  ]
} as WorkProp

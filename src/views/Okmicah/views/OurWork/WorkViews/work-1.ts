import { WorkProp } from './type'
import Img1 from '@/assets/okmicah/client-work/img-client-work-2.png'
import Img2 from '@/assets/okmicah/client-work/img-client-work-3.png'
import Img3 from '@/assets/okmicah/client-work/img-client-work-6.png'
import Img4 from '@/assets/okmicah/client-work/img-client-work-9.png'
import Img5 from '@/assets/okmicah/client-work/img-client-work-10.png'

export default {
  title: 'Pound Cake Cosmetics',
  paragraph: 'F*cking up the beauty industry one red lip at a time. Pound Cake is a beauty brand owned and operated by Camille Belle and Johnny Velazquez in Philadelphia. Born out of a marginalizing undustry that refuses to be inclusive, they built something never before seen and long overdue: liquid lipsticks made for five different skin and lip tones. At Pound Cake, they believe a red lip is bold, empowered, and filled with desire and passion. It’s life, it’s blood, it’s energy. A person with a red lip is absolutely undeniable.',
  whatWeDid: [1, 3],
  howWeDidIt: 'Camille came to us wanting a custom Shopify site that effectively communicates Pound Cakes\'s inclusive and proud attitude. We created a website with purposeful movement and intriguing interactions to evoke a sense of excitement. After launching, Pound Cake sold out of product in 48 hours!!! The continued reaction to the site is incredible, and we\'re so proud of them for their continued success.',
  bannerImage: Img1,
  images: [
    Img2,
    Img3,
    Img4,
    Img5
  ]
} as WorkProp

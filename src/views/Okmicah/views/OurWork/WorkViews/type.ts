export enum WhatWeDid {
  'Web Design' = 1,
  'Brand Photography' = 2,
  'Shopify Development' = 3,
}

export interface WorkProp {
  title: string;
  paragraph: string;
  whatWeDid: WhatWeDid[];
  howWeDidIt: string;
  images: string[];
  bannerImage?: string;
  bannerIframe?: string;
}

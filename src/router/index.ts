import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'

Vue.use(VueRouter)

const context = require.context('./', false, /\.ts$/)
let routes: Array<RouteConfig> = []

context
  .keys()
  .forEach((fileName: string) => {
    if (fileName === './index.ts') return
    routes = [...routes, ...context(fileName).default]
  })

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
  scrollBehavior: () => {
    return {
      x: 0,
      y: 0
    }
  }
})

export default router

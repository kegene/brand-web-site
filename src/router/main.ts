import { RouteConfig } from 'vue-router'

import Page404 from '../views/Page404.vue'
export default [
  {
    path: '/',
    name: 'Page404',
    component: Page404
  }
] as RouteConfig[]

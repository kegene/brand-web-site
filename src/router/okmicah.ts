import { RouteConfig } from 'vue-router'

export default [
  {
    path: '/okmicah',
    name: 'okmicah',
    redirect: '/okmicah',
    component: (): Promise<typeof import('*.vue')> => import(/* webpackChunkName: "okmicah" */ '@/views/Okmicah'),
    children: [
      {
        path: '',
        name: 'OkmicahMain',
        component: (): Promise<typeof import('*.vue')> => import(/* webpackChunkName: "OkmicahMain" */ '@/views/Okmicah/views/Main')
      },
      {
        path: 'OurStory',
        name: 'OkmicahOurStory',
        component: (): Promise<typeof import('*.vue')> => import(/* webpackChunkName: "OkmicahOurStory" */ '@/views/Okmicah/views/OurStory')
      },
      {
        path: 'OurWork',
        name: 'OkmicahOurWork',
        component: (): Promise<typeof import('*.vue')> => import(/* webpackChunkName: "OkmicahOurWork" */ '@/views/Okmicah/views/OurWork')
      },
      {
        path: 'OurWorkViews/:id',
        name: 'OkmicahOurWorkViews',
        component: (): Promise<typeof import('*.vue')> => import(/* webpackChunkName: "OkmicahWorkView" */ '@/views/Okmicah/views/OurWork/WorkView.vue')
      },
      {
        path: 'Services',
        name: 'OkmicahServices',
        component: (): Promise<typeof import('*.vue')> => import(/* webpackChunkName: "OkmicahServices" */ '@/views/Okmicah/views/Services')
      },
      {
        path: 'WorkWithUs',
        name: 'OkmicahWorkWithUs',
        component: (): Promise<typeof import('*.vue')> => import(/* webpackChunkName: "OkmicahWorkWithUs" */ '@/views/Okmicah/views/WorkWithUs')
      }
    ]
  }
] as RouteConfig[]

/* eslint @typescript-eslint/no-var-requires: "off" */
/**
 * vue-cli-service build 的 process.env.NODE_ENV 默認為 production，
 * 所以不必到 package.json 的 script 新增ENV設定
 */
const publicPath = process.env.NODE_ENV === 'production'
  ? '/website-202201/'
  : process.env.NODE_ENV === 'test' ? '/dist'
    : '/'

const css = {
  loaderOptions: {
    // 给 sass-loader 传递选项
    scss: {
      // @/ 是 src/ 的别名
      // 所以这里假设你有 `src/variables.sass` 这个文件
      // 注意：在 sass-loader v8 中，这个选项名是 "prependData"
      prependData: '@import "@/styles/config/_index.scss";'
    }
  }
}

module.exports = {
  publicPath,
  css
}
